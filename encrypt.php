<?php
function encrypt_data($data)
{
	$algorithm = MCRYPT_3DES;			// "rijndael-128" OR called AES
	$my_key = base64_encode(md5("sample_key_for_encryption",true));
	$iv_length = mcrypt_get_iv_size( $algorithm, MCRYPT_MODE_CBC );
	
	$iv = mcrypt_create_iv( $iv_length, MCRYPT_RAND );
	echo '<br>iv: '.$iv;
	$secret = mcrypt_encrypt($algorithm, $my_key, $data, MCRYPT_MODE_CBC, $iv);
	return base64_encode($secret);
}

function decrypt_data($data)
{
	$algorithm = MCRYPT_3DES;			// "rijndael-128" OR called AES
	$my_key = base64_encode(md5("sample_key_for_encryption",true));
	$iv_length = mcrypt_get_iv_size( $algorithm, MCRYPT_MODE_CBC );
	
	$iv = mcrypt_create_iv( $iv_length, MCRYPT_RAND );
	$decode_data = base64_decode($data);
	//$iv = substr( $data,0, $iv_length );
	
	echo '<br>iv: '.$iv;
	
	//$encrypted = substr( $data, $iv_length );
	$original_data = mcrypt_decrypt($algorithm, $my_key, $decode_data, MCRYPT_MODE_CBC, $iv);
	return $original_data;
}



function encrypt( $string ) {
  $algorithm = MCRYPT_RIJNDAEL_256; 
  $key = base64_encode(md5( "sample_key_for_encryption", true)); 
  $iv_length = mcrypt_get_iv_size( $algorithm, MCRYPT_MODE_CBC );
  $iv = mcrypt_create_iv( $iv_length, MCRYPT_RAND );
  echo '<br>iv2: '.$iv;
  $encrypted = mcrypt_encrypt( $algorithm, $key, $string, MCRYPT_MODE_CBC, $iv );
  $result = base64_encode( $iv . $encrypted );
  return $result;
}

function decrypt( $string ) {
  $algorithm =  MCRYPT_RIJNDAEL_256;
  $key = base64_encode(md5( "sample_key_for_encryption", true ));
  $iv_length = mcrypt_get_iv_size( $algorithm, MCRYPT_MODE_CBC );
  $string = base64_decode( $string );
  $iv = substr( $string, 0, $iv_length );
  echo '<br>iv2: '.$iv;
  $encrypted = substr( $string, $iv_length );
  $result = mcrypt_decrypt( $algorithm, $key, $encrypted, MCRYPT_MODE_CBC, $iv );
  return $result;
}


$data = "BrIj$-7127";
$secret = encrypt_data($data);
$original_data = decrypt_data($secret);

$secret2 = encrypt($data);
$original_data2 = decrypt($secret2);

echo "<br>Data for encryption: ".$data; 

echo "<br>Encrypted Data1: ".$secret;
echo "<br>Decrypted Data1: ".$original_data;

echo "<br><br><br><br>Encrypted Data2: ".$secret2;
echo "<br>Decrypted Data2: ".$original_data2;


echo "<br><br><br><br>Base64 Encrypted Data: ".$encode = base64_encode($data);
echo "<br>Base64 Decrypted Data: ".base64_decode($encode);
